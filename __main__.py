import csv
import math

from integral import calc_trapezoid
from math import exp, trunc


def read_from_text_file(filename: str) -> tuple[bool, int, int, float]:
    """
    lê de um ficheiro em formato CSV a primeira linha, formato int1;int2;float1
    :param filename: nome do ficheiro a ler
    :return: True se conseguiu ler, False se não conseguiu, integer1, integer2, float1
    """
    int1 = 0
    int2 = 0
    float1 = 0.0
    result = False

    with open(filename, "r") as f:
        reader = csv.reader(f, delimiter=";")
        for r in reader:
            int1 = int(r[0])
            int2 = int(r[1])
            float1 = float(r[2])
            result = True

    return result, int1, int2, float1


def solve_problem_1():
    """
    Solve problem 1
    :return: the integral value of e^-t for 0<= t <= 2
    """
    # considering that the integral of f(x) = e^x is e^x, then the integral of f(t) = e^-t is e^-t
    # and that the integral of 0 is 0
    return calc_trapezoid(lambda x: 0 if x <= 0 else exp(-x), 0, 0.2, 10)


def solve_problem_2() -> tuple[int, int, float, float]:
    """
    solve problem 2
    :return: [initial infected number, months to consider, amplitude for the trapezoid method, result of the integral]
    """
    r, i, m, d = read_from_text_file("./problema2.csv")
    # if read was successful and d is not zero
    if r and d > 0.0:
        steps = round(m/d)
        # I[K*f(x)] = K*I[f(x)]
        result = i + math.ceil(calc_trapezoid(lambda x: 230*exp(0.3 * x), 0, d, steps))
    else:
        result = math.nan
    return i, m, d, result


def solve_problem_3() -> tuple[int, int, float, float]:
    """
    solve problem 3
    :return: [initial year for maintenance, final year for maintenance, amplitude for the trapezoid method, result of the integral]
    """
    r, a, b, d = read_from_text_file("./problema3.csv")
    # if read was successful and d is not zero
    if r and d > 0.0:
        steps = round((b-a)/d)
        # considera-se que o I  [f(x)+g(x)] = I[f(x)] + I[g(x)]
        result = math.ceil(calc_trapezoid(lambda x: (x**3)/3+(x**2)*5/2, a, d, steps))
    else:
        result = math.nan
    return a, b, d, result


def main():
    """
    main function
    :return: none
    """

    # solves problem1
    print("PROBLEMA1:")
    print("A probabilibade pretendida para o problema 1 é :", solve_problem_1(), "\n")

    # solves problem2
    print("PROBLEMA2:")
    infetados0, meses, amplitude, projecao = solve_problem_2()
    texto = "Com um nº inicial de infectados {i}, ao fim de {m} meses, estimam-se {n} infetados, calculado pelo método do trapézio com amplitude {d}"
    print(texto.format(i=str(infetados0), m=str(meses), n=str(projecao), d=amplitude), "\n")

    # solves problem2
    print("PROBLEMA3:")
    ano_a, ano_b, amplitude, orcamento = solve_problem_3()
    texto = "Entre o {a}º e o {b}º anos de utilização do equipamento, estima-se um aumento dos custos de manutenção em {n} (unidades?), calculado pelo método do trapézio com amplitude {d}"
    print(texto.format(a=str(ano_a), b=str(ano_b), n=str(orcamento), d=amplitude), "\n")


if __name__ == "__main__":
    main()
