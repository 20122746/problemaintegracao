from functools import reduce


def calc_value(expression, f):
    """
    Generic function to calculate expression results using lambdas
    :param expression: lambda expression to test
    :param f: value to test the expression with
    :return: the result of expression(f)
    """
    return expression(f)


def calc_trapezoid(f, lower: float, h: float, steps: int) -> float:
    """
    Calculate the value of an integral using the trapezoid method
    :param f: lambda expression representing the integral function to use
    :param lower: the lower value to start the integration
    :param h: range of each interval
    :param steps: how many blocks to break the trapezoid into
    :return: the value of the approximate value of the integral
    """

    # creates a list with the lower value of the variable
    values = [lower]

    # appends to the list the steps of x, using h as the amplitude
    for s in range(steps):
        values.append(lower+h*(s+1))

    # use the function map to calculate the value of f(x) for each step
    values = list(map(f, values))

    # calculates the value of the integral using the trapezoid method
    # uses the function reduce() to calculate the sum of f(x0)+ 2*f(x1)+...+2*f(xn-1)
    result = (reduce(lambda a, b: a+2*b, values[0:-1]) + values[-1])*h/2

    # returns the result
    return result


